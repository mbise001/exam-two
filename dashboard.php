<?php
require('db.php');
include("auth.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Dashboard</title>
<link rel="stylesheet" type="text/css" href="style.css"/>
</head>
<body>

<p><h2>Welcome to your Dashboard <?php echo $_SESSION['username']; ?></h2></p>
<ul><li><p><a href="logout.php">Logout</a></p></li>
<li><p><a href="view.php">View Records</a><p></li>
<li><p><a href="insert.php">Insert New Record</a></p></li>
<li><p><a href="index.php">Home</a></p></li>
</ul>

</div>
</body>
</html>